﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MathNet.Numerics.LinearAlgebra;

namespace Cosig
{
    public abstract class SceneObject
    {
        public int material;
        public int transformation;
        protected double[,] tmatrix;
        protected double[,] tmatrixInv;
        protected double[,] tmatrixInvTrans;
        public abstract void setTMatrix(List<Transformation> transform, double[,] tcam);

        public double[,] getTMatrix() { return this.tmatrix; }
        public double[,] getTMatrixInversed() { return this.tmatrixInv; }
        public double[,] getTMatrixInvTransposed() { return this.tmatrixInvTrans; }
    }
}
