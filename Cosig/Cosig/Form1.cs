﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Globalization;
using System.IO;
using System.Windows.Media.Media3D;
using MathNet.Numerics.LinearAlgebra;
using System.Threading;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;



namespace Cosig
{
    public partial class Form1 : Form
    {

        private Sphere sphere;
        private List<SceneObject> sceneObjects = new List<SceneObject>();
        private Camera camera;
        private Box box;
        private List<List<Transformation>> transformations = new List<List<Transformation>>();
        private List<Light> lights = new List<Light>();
        private Scene scene;
        private List<Triangles> triangleShapes = new List<Triangles>();
        private List<Material> materials = new List<Material>();
        private Ray origRay;
        const float bias = 0.0001f;
        private double origRefract;

        public Form1()
        {
            InitializeComponent();
        }

        private void processFile(String fileName)
        {
            var lines = File.ReadLines(fileName);
            for (int i = 0; i < lines.Count(); i++)
            {
                string line = lines.ElementAt(i);
                if (lines.ElementAt(i) != String.Empty)
                {
                    switch (line.Trim())
                    {

                        case "Camera":
                            parseCamera(lines, i);
                            i = i + 6;
                            break;

                        case "Box":
                            parseBox(lines, i);
                            i = i + 5;
                            break;

                        case "Sphere":
                            parseSphere(lines, i);
                            i = i + 5;
                            break;

                        case "Light":
                            parseLight(lines, i);
                            i = i + 5;
                            break;

                        case "Image":
                            parseImage(lines, i);
                            i = i + 5;
                            break;

                        case "Triangles":
                            i = i + parseTriangles(lines, i);
                            break;

                        case "Transformation":
                            i = i + parseTransformation(lines, i);
                            break;

                        case "Material":
                            parseMaterial(lines, i);
                            i = i + 5;
                            break;

                        default:
                            break;
                    }
                }
            }

        }

        private void start_callback()
        {
            Bitmap bm = new Bitmap(scene.resolution[0], scene.resolution[1]);
            for (int j = 0; j < scene.resolution[1]; j++)
            {
                for (int i = 0; i < scene.resolution[0]; i++)
                {
                    origRefract = 1.00001;
                    //primary_ray(i, j);

                    //Color c = new Color();

                    //Bitmap bm = new Bitmap(scene.resolution[0], scene.resolution[1]);

                    //c = traceRay(origRay, (int)numericUpDown1.Value);

                    {
                        origRay = new Ray(primary_ray(i, j), new Vector3D(0, 0, camera.distance));

                        Color color = trace_ray(origRay, (int)numericUpDown1.Value);

                        bm.SetPixel(i, scene.resolution[1] - j - 1, color);
                    }

                    //criar classe color propria com floats red, blue green entre 0-1
                    //c = trace_ray(origRay, (int) numericUpDown1.Value);
                    // limitar c (se componente rgb > 1, componente =1)
                    //pixel[scene.resolution[1] - j -1] [i] = r * 255, g * 255, b * 255

                }
            }
            pictureBox1.BackgroundImage = bm;
        }

        private Vector3D primary_ray(int i, int j)
        {
            double height = 2 * camera.distance * Math.Tan(camera.fieldOfView / 2 * Math.PI / 180);

            //hres -> scene.resolution[0]
            //vres -> scene.resolution[1]
            double width = height * scene.resolution[0] / scene.resolution[1];

            double Px = (i + 0.5) * width / scene.resolution[0] - width / 2;

            double Py = (j + 0.5) * height / scene.resolution[1] - height / 2;

            Vector3D direction = new Vector3D(Px, Py, -camera.distance);
            direction.Normalize();
            return direction;
        }

        private Color trace_ray(Ray ray, int value)
        {
            double t = -1;
            double realt = -1;
            double mlenght = double.MaxValue;
            double auxReal = double.MaxValue;
            double[] colore = new double[] { 0, 0, 0 };
            Point3D interPoint = new Point3D();
            SceneObject obj = new Sphere(0, 0);
            Vector3D auxNormal = new Vector3D();
            Vector3D normal = new Vector3D();
            Ray rL = new Ray(new Vector3D(0, 0, 0), new Vector3D(0, 0, 0));
            Color reflCol = Color.FromArgb(0, 0, 0);
            Color refrCol = Color.FromArgb(0, 0, 0);
            bool intersect = false;

            foreach (SceneObject sceneObject in sceneObjects)
            {
                Transformatrix auxMatrix = new Transformatrix();

                auxMatrix.setTransformatrix(sceneObject.getTMatrixInversed());

                double[] auxPoint = auxMatrix.multiply1(new double[] { ray.origin.X, ray.origin.Y, ray.origin.Z, 1 });
                auxMatrix.setTransformatrix(sceneObject.getTMatrixInversed());
                double[] auxVec = auxMatrix.multiply1(new double[] { ray.direction.X, ray.direction.Y, ray.direction.Z, 0 });
                Vector3D nDirection = new Vector3D(auxVec[0], auxVec[1], auxVec[2]);
                Vector3D nOrigin = new Vector3D(auxPoint[0] / auxPoint[3], auxPoint[1] / auxPoint[3], auxPoint[2] / auxPoint[3]);
                nDirection.Normalize();

                if (closest_interception(new Ray(nDirection, nOrigin), sceneObject, out t, out realt, out auxNormal))
                {
                    if (realt < auxReal)
                    {
                        rL.direction = nDirection;
                        rL.origin = nOrigin;
                        mlenght = t;
                        auxReal = realt;
                        obj = sceneObject;
                        normal = auxNormal;
                        intersect = true;
                    }
                }

            }
            
            if (intersect)
            {
                Material mat = materials.ElementAt(obj.material);
                normal.Normalize();
                
                foreach (Light light in lights)
                {
                    colore = AmbientLight(colore, mat.color, light.intensity, mat.getAmbientCoef());

                    Transformatrix auxMatrix = new Transformatrix();
                    auxMatrix.setTransformatrix(obj.getTMatrix());

                    Vector3D pL = rL.pointAtParameter(mlenght);
                    double[] auxPoint = auxMatrix.multiply1(new double[] { pL.X, pL.Y, pL.Z, 1 });



                    interPoint = new Point3D(auxPoint[0] / auxPoint[3], auxPoint[1] / auxPoint[3], auxPoint[2] / auxPoint[3]);

                    Vector3D vecL = light.getOrigin() - interPoint;
                    double length = vecL.Length;
                    vecL.Normalize();

                    Ray shadowray = new Ray(vecL, new Vector3D(interPoint.X, interPoint.Y, interPoint.Z) + normal * bias);

                    Vector3D norm = new Vector3D();
                    double t3 = -1;
                    bool inter = false;
                    double ts = double.MaxValue;
                    double realt3 = -1;
                    foreach (SceneObject sceneObject in sceneObjects)
                    {

                        Transformatrix tempMatrix = new Transformatrix();
                        tempMatrix.setTransformatrix(sceneObject.getTMatrixInversed());

                        auxPoint = tempMatrix.multiply1(new double[] { shadowray.origin.X, shadowray.origin.Y, shadowray.origin.Z, 1 });
                        tempMatrix.setTransformatrix(sceneObject.getTMatrixInversed());
                        double[] auxVec = tempMatrix.multiply1(new double[] { shadowray.direction.X, shadowray.direction.Y, shadowray.direction.Z, 0 });
                        Vector3D newDirection = new Vector3D(auxVec[0], auxVec[1], auxVec[2]);
                        Vector3D newOrigin = new Vector3D(auxPoint[0] / auxPoint[3], auxPoint[1] / auxPoint[3], auxPoint[2] / auxPoint[3]);
                        newDirection.Normalize();
                        
                        if (closest_interception(new Ray(newDirection, newOrigin), sceneObject, out t3, out realt3, out norm))
                        {
                            if (t3 < ts)
                            {
                                ts = t3;
                                inter = true;
                                break;
                            }
                        }
                    }
                    
                    if (!inter || ts < 0.000001 || ts > length)
                    {
                        double shade = Vector3D.DotProduct(vecL, normal);
                        if (shade > 0)
                        { 
                            colore = DiffuseLight(colore, mat.color, light.intensity, mat.getDiffuseCoef(), shade);
                        }
                    }
                }

                if (value > 0)
                {
                    if (mat.getReflectionCoef() > 0)
                    {
                        Vector3D refl = calculateRefletive(normal, ray.direction);
                        Ray reflectray = new Ray(refl, new Vector3D(interPoint.X, interPoint.Y, interPoint.Z) + normal * bias);
                        Color auxCol = trace_ray(reflectray, value - 1);
                        reflCol = Color.FromArgb((int)(auxCol.R * mat.getReflectionCoef()), (int)(auxCol.G * mat.getReflectionCoef()), (int)(auxCol.B * mat.getReflectionCoef()));
                    }
                    if (mat.getRefractionCoef() > 0)
                    {
                        Vector3D dir = calculateRefracted(normal, ray.direction, mat.getRefraction());
                        if (dir.X != 0 && dir.Y != 0 && dir.Z != 0)
                        {
                            Ray refractedray = new Ray(dir, new Vector3D(interPoint.X, interPoint.Y, interPoint.Z));
                            Color auxCol = trace_ray(refractedray, value - 1);
                            refrCol = Color.FromArgb((int)(auxCol.R * mat.getRefractionCoef()), (int)(auxCol.G * mat.getRefractionCoef()), (int)(auxCol.B * mat.getRefractionCoef()));
                        }
                    }
                }

            }
            else
            {
                intersect = false;
                colore = scene.backgroundColor;
            }
            
            int red = (int)(colore[0] * 255) + (int)((intersect ? refrCol.R * materials.ElementAt(obj.material).color[0] : 0)) + (int)((intersect ? reflCol.R * materials.ElementAt(obj.material).color[0] : 0));

            int green = (int)(colore[1] * 255) + (int)((intersect ? refrCol.G * materials.ElementAt(obj.material).color[1] : 0)) + (int)((intersect ? reflCol.G * materials.ElementAt(obj.material).color[1] : 0));

            int blue = (int)(colore[2] * 255) + (int)((intersect ? refrCol.B * materials.ElementAt(obj.material).color[2] : 0)) + (int)((intersect ? reflCol.B * materials.ElementAt(obj.material).color[2] : 0));

            return Color.FromArgb(red > 255 ? 255 : red, green > 255 ? 255 : green, blue > 255 ? 255 : blue);
        }

        Vector3D calculateRefletive(Vector3D normal, Vector3D direction)
        {
            //(c1) = N * V
            double c1 = Vector3D.DotProduct(normal, direction);

            Vector3D rl = direction + (-2 * c1 * normal);
            rl.Normalize();
            return rl;
        }

        Vector3D calculateRefracted(Vector3D normal, Vector3D direction, double refraction)
        {
            double n = origRefract / (refraction == origRefract ? 1.00001 : refraction);
            double c1 = Vector3D.DotProduct(normal, -direction);
            double aux = 1 - Math.Pow(n, 2) * (1 - Math.Pow(c1, 2));
            if (aux > 0)
            {
                double c2 = Math.Sqrt(aux);
                Vector3D rr = (n * direction) + (n * c1 - c2) * normal;
                origRefract = refraction;
                rr.Normalize();
                return rr;
            }
            else
            {
                return new Vector3D(0, 0, 0);
            }

        }

        private double[] DiffuseLight(double[] orig, double[] b, double[] c, double coeff, double shade)
        {
            orig[0] += b[0] * c[0] * coeff * shade;
            orig[1] += b[1] * c[1] * coeff * shade;
            orig[2] += b[2] * c[2] * coeff * shade;
            return orig;
        }

        public static double[] AmbientLight(double[] orig, double[] b, double[] c, double coeff)
        {
            orig[0] += b[0] * c[0] * coeff;
            orig[1] += b[1] * c[1] * coeff;
            orig[2] += b[2] * c[2] * coeff;
            return orig;
        }

        private bool closest_interception(Ray ray, SceneObject sceneObject, out double t, out double realt, out Vector3D normal)
        {
            if (sceneObject is Sphere)
            {
                Sphere sphere = sceneObject as Sphere;

                double a = Math.Pow(ray.direction.X, 2) + Math.Pow(ray.direction.Y, 2) + Math.Pow(ray.direction.Z, 2);
                double b = 2 * (ray.direction.X * ray.origin.X + ray.direction.Y * ray.origin.Y + ray.direction.Z * ray.origin.Z);
                double c = Math.Pow(ray.origin.X, 2) + Math.Pow(ray.origin.Y, 2) + Math.Pow(ray.origin.Z, 2) - 1;
                double delta = Math.Pow(b, 2) - 4 * a * c;
                if (delta < 0)
                {
                    t = -1;
                    realt = -1;
                    normal = new Vector3D();
                    return false;
                }
                else
                {
                    double t1 = (-b - Math.Sqrt(delta)) / 2 * a;
                    double t2 = (-b + Math.Sqrt(delta)) / 2 * a;

                    Transformatrix auxMatrix = new Transformatrix();

                    auxMatrix.setTransformatrix(sceneObject.getTMatrix());

                    if (t1 > t2 && t2 > 0)
                    {
                        Vector3D pL = ray.pointAtParameter(t1);
                        double[] auxPoint = auxMatrix.multiply1(new double[] { pL.X, pL.Y, pL.Z, 1 });


                        realt = Math.Sqrt(Math.Pow(auxPoint[0] / auxPoint[3] - origRay.origin.X, 2) + Math.Pow(auxPoint[1] / auxPoint[3] - origRay.origin.Y, 2) + Math.Pow(auxPoint[2] / auxPoint[3] - origRay.origin.Z, 2));

                        auxMatrix.setTransformatrix(sceneObject.getTMatrixInvTransposed());
                        double[] auxnormal = auxMatrix.multiply1(new double[] { ray.origin.X + (t2 * ray.direction.X), ray.origin.Y + (t2 * ray.direction.Y), ray.origin.Z + (t2 * ray.direction.Z), 0 });

                        normal = new Vector3D(auxnormal[0], auxnormal[1], auxnormal[2]);

                        t = t2;
                        return true;
                    }
                    if (t2 > t1 && t1 > 0)
                    {
                        Vector3D pL = ray.pointAtParameter(t1);
                        double[] auxPoint = auxMatrix.multiply1(new double[] { pL.X, pL.Y, pL.Z, 1 });


                        realt = Math.Sqrt(Math.Pow(auxPoint[0] / auxPoint[3] - origRay.origin.X, 2) + Math.Pow(auxPoint[1] / auxPoint[3] - origRay.origin.Y, 2) + Math.Pow(auxPoint[2] / auxPoint[3] - origRay.origin.Z, 2));

                        auxMatrix.setTransformatrix(sceneObject.getTMatrixInvTransposed());
                        double[] auxnormal = auxMatrix.multiply1(new double[] { ray.origin.X + (t1 * ray.direction.X), ray.origin.Y + (t1 * ray.direction.Y), ray.origin.Z + (t1 * ray.direction.Z), 0 });

                        normal = new Vector3D(auxnormal[0], auxnormal[1], auxnormal[2]);

                        t = t1;
                        return true;
                    }
                }
            }
            if (sceneObject is Triangle)
            {

                Triangle p = sceneObject as Triangle;
                Point3D p_a = new Point3D(p.vertex1[0], p.vertex1[1], p.vertex1[2]);
                Point3D p_b = new Point3D(p.vertex2[0], p.vertex2[1], p.vertex2[2]);
                Point3D p_c = new Point3D(p.vertex3[0], p.vertex3[1], p.vertex3[2]);

                double[,] matrix = new double[3, 3];

                matrix = new double[,]{
                    { p_a.X - p_b.X, p_a.X - p_c.X,ray.direction.X},
                    { p_a.Y - p_b.Y, p_a.Y - p_c.Y,ray.direction.Y},
                    { p_a.Z - p_b.Z, p_a.Z - p_c.Z,ray.direction.Z}
                };

                Matrix<double> A = Matrix<double>.Build.DenseOfArray(matrix);
                double area = A.Determinant();

                matrix = new double[,]{
                    { p_a.X - ray.origin.X, p_a.X - p_c.X,ray.direction.X},
                    { p_a.Y - ray.origin.Y, p_a.Y - p_c.Y,ray.direction.Y},
                    { p_a.Z - ray.origin.Z, p_a.Z - p_c.Z,ray.direction.Z}
                };

                A = Matrix<double>.Build.DenseOfArray(matrix);

                double beta = A.Determinant() / area;

                matrix = new double[,]{
                    { p_a.X - p_b.X, p_a.X - ray.origin.X, ray.direction.X},
                    { p_a.Y - p_b.Y, p_a.Y - ray.origin.Y, ray.direction.Y},
                    { p_a.Z - p_b.Z, p_a.Z - ray.origin.Z, ray.direction.Z}
                };

                A = Matrix<double>.Build.DenseOfArray(matrix);

                double gamma = A.Determinant() / area;

                matrix = new double[,]{
                    { p_a.X - p_b.X, p_a.X - p_c.X, p_a.X - ray.origin.X},
                    { p_a.Y - p_b.Y, p_a.Y - p_c.Y, p_a.Y - ray.origin.Y},
                    { p_a.Z - p_b.Z, p_a.Z - p_c.Z, p_a.Z - ray.origin.Z}
                };

                A = Matrix<double>.Build.DenseOfArray(matrix);

                double theta = A.Determinant() / area;

                if (beta + gamma < 1 && beta > 0 && gamma > 0 && theta > 0.0000001)
                {
                    Vector3D N = Vector3D.CrossProduct(p_b - p_a, p_c - p_a);

                    Transformatrix auxMatrix = new Transformatrix();
                    auxMatrix.setTransformatrix(sceneObject.getTMatrix());

                    Vector3D pL = ray.pointAtParameter(theta);
                    double[] auxPoint = auxMatrix.multiply1(new double[] { pL.X, pL.Y, pL.Z, 1 });


                    realt = Math.Sqrt(Math.Pow(auxPoint[0] / auxPoint[3] - origRay.origin.X, 2) + Math.Pow(auxPoint[1] / auxPoint[3] - origRay.origin.Y, 2) + Math.Pow(auxPoint[2] / auxPoint[3] - origRay.origin.Z, 2));

                    auxMatrix.setTransformatrix(sceneObject.getTMatrixInvTransposed());
                    double[] auxnormal = auxMatrix.multiply1(new double[] { N.X, N.Y, N.Z, 0 });

                    normal = new Vector3D(auxnormal[0], auxnormal[1], auxnormal[2]);

                    t = theta;

                    if (Vector3D.DotProduct(N, ray.direction) > 0)
                    {
                        normal = -normal;
                    }
                    return true;
                }
                else
                {
                    t = -1;
                    realt = -1;
                    normal = new Vector3D();
                    return false;
                }
            }
            if (sceneObject is Box)
            {
                double X1 = -.5;
                double X2 = .5;
                double Y1 = -.5;
                double Y2 = .5;
                double Z1 = -.5;
                double Z2 = .5;

                double tnear;
                double tfar;

                t = -1;
                realt = -1;
                normal = new Vector3D();
                int tnear_index = 0; int tfar_index = 0;

                //test if parallel for X
                if (ray.direction.X == 0 && (ray.origin.X < X1 || ray.origin.X > X2))
                {
                    t = -1;
                    realt = -1;
                    normal = new Vector3D();
                    return false;
                }
                //if not parallel
                else
                {
                    tnear = (X1 - ray.origin.X) / ray.direction.X;
                    tfar = (X2 - ray.origin.X) / ray.direction.X;

                    if (tnear > tfar)
                    {
                        double temp = tnear;
                        tnear = tfar;
                        tfar = temp;
                    }

                    if (tnear > tfar)
                    {
                        t = -1;
                        realt = -1;
                        normal = new Vector3D();
                        return false;
                    }
                    if (tfar < 0)
                    {
                        t = -1;
                        realt = -1;
                        normal = new Vector3D();
                        return false;
                    }
                }

                //test if parallel for Y
                if (ray.direction.Y == 0 && (ray.origin.Y < Y1 || ray.origin.Y > Y2))
                {
                    t = -1;
                    realt = -1;
                    normal = new Vector3D();
                    return false;
                }
                //if not parallel
                else
                {
                    double t1 = (Y1 - ray.origin.Y) / ray.direction.Y;
                    double t2 = (Y2 - ray.origin.Y) / ray.direction.Y;

                    if (t1 > t2)
                    {
                        double temp = t1;
                        t1 = t2;
                        t2 = temp;
                    }

                    if (t1 > tnear)
                    {
                        tnear = t1;
                        tnear_index = 1;
                    }

                    if (t2 < tfar)
                    {
                        tfar = t2;
                        tfar_index = 1;
                    }

                    if (tnear > tfar)
                    {
                        t = -1;
                        realt = -1;
                        normal = new Vector3D();
                        return false;
                    }
                    if (tfar < 0)
                    {
                        t = -1;
                        realt = -1;
                        normal = new Vector3D();
                        return false;
                    }
                }

                //test if parallel for Z
                if (ray.direction.Z == 0 && (ray.origin.Z < Z1 || ray.origin.Z > Z2))
                {
                    t = -1;
                    realt = -1;
                    normal = new Vector3D();
                    return false;
                }

                //if not
                else
                {
                    double t1 = (Z1 - ray.origin.Z) / ray.direction.Z;
                    double t2 = (Z2 - ray.origin.Z) / ray.direction.Z;

                    if (t1 > t2)
                    {
                        double temp = t1;
                        t1 = t2;
                        t2 = temp;
                    }

                    if (t1 > tnear)
                    {
                        tnear = t1;
                        tnear_index = 2;
                    }

                    if (t2 < tfar)
                    {
                        tfar = t2;
                        tfar_index = 2;
                    }

                    if (tnear > tfar)
                    {
                        t = -1;
                        realt = -1;
                        normal = new Vector3D();
                        return false;
                    }
                    if (tfar < 0)
                    {
                        t = -1;
                        realt = -1;
                        normal = new Vector3D();
                        return false;
                    }
                    Vector3D tnormal = new Vector3D();
                    Vector3D[] normals = new Vector3D[] { new Vector3D(1, 0, 0), new Vector3D(0, 1, 0), new Vector3D(0, 0, 1) };
                    if (tnear <= 0)
                    {
                        t = tfar;
                        tnormal = normals[tfar_index];
                    }
                    else
                    {
                        t = tnear;
                        tnormal = normals[tnear_index];
                    }

                    double auxt = tnear <= 0 ? tfar : tnear;

                    Transformatrix auxMatrix = new Transformatrix();

                    auxMatrix.setTransformatrix(sceneObject.getTMatrix());
                    Vector3D pL = ray.pointAtParameter(auxt);
                    double[] auxPoint = auxMatrix.multiply1(new double[] { pL.X, pL.Y, pL.Z, 1 });

                    realt = Math.Sqrt(Math.Pow(auxPoint[0] / auxPoint[3] - origRay.origin.X, 2) + Math.Pow(auxPoint[1] / auxPoint[3] - origRay.origin.Y, 2) + Math.Pow(auxPoint[2] / auxPoint[3] - origRay.origin.Z, 2));
                    t = auxt;

                    auxMatrix.setTransformatrix(sceneObject.getTMatrixInvTransposed());
                    double[] auxnormal = auxMatrix.multiply1(new double[] { tnormal.X, tnormal.Y, tnormal.Z, 0 });

                    normal = new Vector3D(auxnormal[0], auxnormal[1], auxnormal[2]);
                    double dot = Vector3D.DotProduct(tnormal, ray.direction);
                    if (dot > 0)
                    {
                        normal = -normal;
                    }
                    return true;
                }
            }
            t = -1;
            realt = -1;
            normal = new Vector3D();
            return false;
        }

        public void ReadFile()
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Text Files|*.txt";
            if (open.ShowDialog() == DialogResult.OK)
            {
                if (open.FileName.EndsWith(".txt"))
                {

                    String bigFile = open.FileName.ToString();

                    FileInfo fileSize = new FileInfo(bigFile);
                    long size = fileSize.Length;
                    long currentSize = 0;
                    long incrementSize = (size / 100);

                    StreamReader stream = new StreamReader(new FileStream(bigFile, FileMode.Open));

                    char[] buff = new char[10];

                    while (!stream.EndOfStream)
                    {
                        currentSize += stream.Read(buff, 0, buff.Length);
                        if (currentSize >= incrementSize)
                        {
                            currentSize -= incrementSize;
                        }
                    }
                    if (stream.EndOfStream)
                    {

                        stream.Close();
                        processFile(open.FileName);
                        camera.setTransformation(transformations.ElementAt(camera.transformation));

                        foreach (Light light in lights)
                        {
                            light.setOrigin(transformations.ElementAt(light.transformation), camera.getTMatrix());
                        }

                        foreach (SceneObject sceneObject in sceneObjects)
                        {
                            sceneObject.setTMatrix(transformations.ElementAt(sceneObject.transformation), camera.getTMatrix());
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Can only process Text Files (.txt extension).");
                }

            }
        }

        private void parseSphere(IEnumerable<string> lines, int i)
        {
            int sphereTransformation = int.Parse(lines.ElementAt(i + 2).Trim());
            int sphereMaterial = int.Parse(lines.ElementAt(i + 3).Trim());
            sphere = new Sphere(sphereTransformation, sphereMaterial);
            sceneObjects.Add(sphere);
        }

        private void parseBox(IEnumerable<string> lines, int i)
        {
            int boxTransformation = int.Parse(lines.ElementAt(i + 2).Trim());
            int boxMaterial = int.Parse(lines.ElementAt(i + 3).Trim());
            box = new Box(boxTransformation, boxMaterial);
            sceneObjects.Add(box);
        }

        private void parseCamera(IEnumerable<string> lines, int i)
        {
            int camTransformation = int.Parse(lines.ElementAt(i + 2).Trim());
            double distance;
            double fieldOfView;
            double.TryParse(lines.ElementAt(i + 3).Trim().Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture, out distance);
            double.TryParse(lines.ElementAt(i + 4).Trim().Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture, out fieldOfView);

            camera = new Camera(camTransformation, distance, fieldOfView);
        }

        private void parseLight(IEnumerable<string> lines, int i)
        {
            int lightTransformation = int.Parse(lines.ElementAt(i + 2).Trim());
            double[] lightColor = new double[3];
            var stringLightColor = lines.ElementAt(i + 3).Trim().Split(' ');
            for (int l = 0; l < stringLightColor.Count(); l++)
            {
                double.TryParse(stringLightColor[l].Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture, out lightColor[l]);
            }
            lights.Add(new Light(lightTransformation, lightColor));
        }

        private void parseImage(IEnumerable<string> lines, int i)
        {
            int[] dimensions = new int[2];
            double[] background = new double[3];

            var stringDim = lines.ElementAt(i + 2).Trim().Split(' ');
            for (int l = 0; l < stringDim.Count(); l++)
            {
                dimensions[l] = int.Parse(stringDim[l]);
            }

            var stringBg = lines.ElementAt(i + 3).Trim().Split(' ');
            for (int l = 0; l < stringBg.Count(); l++)
            {
                double.TryParse(stringBg[l].Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture, out background[l]);
            }

            scene = new Scene(dimensions, background);
        }

        private int parseTriangles(IEnumerable<String> lines, int i)
        {
            int triTransformation = int.Parse(lines.ElementAt(i + 2).Trim());
            List<Triangle> triangles = new List<Triangle>();
            int triMaterial;

            int ret = 4;

            for (int k = i + 3; !lines.ElementAt(k).Trim().Contains("}"); k = k + 4)
            {
                double[] firstVec = new double[3];
                double[] secVec = new double[3];
                double[] thirdVec = new double[3];
                triMaterial = int.Parse(lines.ElementAt(k).Trim());
                double.TryParse(lines.ElementAt(k + 1).Trim().Split(' ')[0].Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture, out firstVec[0]);
                double.TryParse(lines.ElementAt(k + 1).Trim().Split(' ')[1].Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture, out firstVec[1]);
                double.TryParse(lines.ElementAt(k + 1).Trim().Split(' ')[2].Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture, out firstVec[2]);

                double.TryParse(lines.ElementAt(k + 2).Trim().Split(' ')[0].Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture, out secVec[0]);
                double.TryParse(lines.ElementAt(k + 2).Trim().Split(' ')[1].Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture, out secVec[1]);
                double.TryParse(lines.ElementAt(k + 2).Trim().Split(' ')[2].Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture, out secVec[2]);

                double.TryParse(lines.ElementAt(k + 3).Trim().Split(' ')[0].Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture, out thirdVec[0]);
                double.TryParse(lines.ElementAt(k + 3).Trim().Split(' ')[1].Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture, out thirdVec[1]);
                double.TryParse(lines.ElementAt(k + 3).Trim().Split(' ')[2].Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture, out thirdVec[2]);

                triangles.Add(new Triangle(triTransformation, triMaterial, firstVec, secVec, thirdVec));
                sceneObjects.Add(new Triangle(triTransformation, triMaterial, firstVec, secVec, thirdVec));
                ret = ret + 4;
            }
            triangleShapes.Add(new Triangles(triTransformation, triangles));
            return ret;
        }

        private int parseTransformation(IEnumerable<String> lines, int i)
        {
            List<Transformation> transformation = new List<Transformation>();
            int ret = 3;

            for (int k = i + 2; !lines.ElementAt(k).Trim().Contains("}"); k++)
            {
                string[] tLine = lines.ElementAt(k).Trim().Split(' ');

                switch (tLine[0])
                {
                    case "S":
                        double[] scale = new double[3];
                        double.TryParse(tLine[1].Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture, out scale[0]);
                        double.TryParse(tLine[2].Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture, out scale[1]);
                        double.TryParse(tLine[3].Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture, out scale[2]);
                        transformation.Add(new Scale(scale));
                        ret++;
                        break;
                    case "T":
                        double[] trans = new double[3];
                        double.TryParse(tLine[1].Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture, out trans[0]);
                        double.TryParse(tLine[2].Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture, out trans[1]);
                        double.TryParse(tLine[3].Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture, out trans[2]);
                        transformation.Add(new Translate(trans));
                        ret++;
                        break;
                    case "Rx":
                        double rx;
                        double.TryParse(tLine[1].Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture, out rx);
                        transformation.Add(new Rotate(rx, 0, 0));
                        ret++;
                        break;
                    case "Ry":
                        double ry;
                        double.TryParse(tLine[1].Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture, out ry);
                        transformation.Add(new Rotate(0, ry, 0));
                        ret++;
                        break;
                    case "Rz":
                        double rz;
                        double.TryParse(tLine[1].Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture, out rz);
                        transformation.Add(new Rotate(0, 0, rz));
                        ret++;
                        break;
                    default:
                        break;
                }
            }

            transformations.Add(transformation);

            return ret;
        }

        private void parseMaterial(IEnumerable<String> lines, int i)
        {
            double[] color = new double[3];
            double[] coefficients = new double[5];
            var stringColor = lines.ElementAt(i + 2).Trim().Split(' ');

            for (int l = 0; l < stringColor.Count(); l++)
            {
                double.TryParse(stringColor[l].Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture, out color[l]);
            }

            var stringLight = lines.ElementAt(i + 3).Trim().Split(' ');
            for (int l = 0; l < stringLight.Count(); l++)
            {
                double.TryParse(stringLight[l].Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture, out coefficients[l]);
            }

            materials.Add(new Material(color, coefficients));
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Text = "COSIG - Ray Tracer";
            this.button2.Click += new System.EventHandler(this.button2_Click);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ReadFile();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            start_callback();
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
