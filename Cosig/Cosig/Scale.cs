﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cosig
{
    class Scale : Transformation
    {
        private double[] scale = new double[3];

        public Scale(double[] scale)
        {
            this.scale = scale;
        }

        public void setScale(double[] scale)
        {
            this.scale = scale;
        }

        public double[] getScale()
        {
            return scale;
        }

    }

}
