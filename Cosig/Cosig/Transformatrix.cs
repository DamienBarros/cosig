﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cosig
{
    public class Transformatrix
    {
        double[,] transformMatrix = new double[4, 4];

        public Transformatrix()
        {
            identityMatrix();
        }

        public double[,] getTransformatrix()
        {
            return this.transformMatrix;
        }

        public void setTransformatrix(double[,] m)
        {
            this.transformMatrix = m;
        }

        public double[] multiply1(double[] pointA)
        {
            int i, j;
            double[] pointB = new double[4];
            for (i = 0; i < 4; i++)
                pointB[i] = 0.0;
            for (i = 0; i < 4; i++)
            {
                for (j = 0; j < 4; j++)
                {
                    pointB[i] += transformMatrix[i, j] * pointA[j];
                }
            }
            return pointB;
        }

        public double[,] multiply3(double[,] matrixA)
        {
            int i, j, k;
            double[,] matrixB = new double[4, 4];

            for (i = 0; i < 4; i++)
                for (j = 0; j < 4; j++)
                {
                    matrixB[i, j] = transformMatrix[i, j];
                    transformMatrix[i, j] = 0.0;
                }
            for (i = 0; i < 4; i++)
            {
                for (j = 0; j < 4; j++)
                {
                    for (k = 0; k < 4; k++)
                    {
                        transformMatrix[i, j] += matrixB[i, k] * matrixA[k, j];
                    }
                }
            }
            return transformMatrix;
        }

        void identityMatrix()
        {
            transformMatrix[0, 0] = 1.0;
            transformMatrix[0, 1] = 0.0;
            transformMatrix[0, 2] = 0.0;
            transformMatrix[0, 3] = 0.0;
            transformMatrix[1, 0] = 0.0;
            transformMatrix[1, 1] = 1.0;
            transformMatrix[1, 2] = 0.0;
            transformMatrix[1, 3] = 0.0;
            transformMatrix[2, 0] = 0.0;
            transformMatrix[2, 1] = 0.0;
            transformMatrix[2, 2] = 1.0;
            transformMatrix[2, 3] = 0.0;
            transformMatrix[3, 0] = 0.0;
            transformMatrix[3, 1] = 0.0;
            transformMatrix[3, 2] = 0.0;
            transformMatrix[3, 3] = 1.0;
        }

        public void translate(double x, double y, double z)
        {
            double[,] translateMatrix = new double[4, 4];

            translateMatrix[0, 0] = 1.0;
            translateMatrix[0, 1] = 0.0;
            translateMatrix[0, 2] = 0.0;
            translateMatrix[0, 3] = x;
            translateMatrix[1, 0] = 0.0;
            translateMatrix[1, 1] = 1.0;
            translateMatrix[1, 2] = 0.0;
            translateMatrix[1, 3] = y;
            translateMatrix[2, 0] = 0.0;
            translateMatrix[2, 1] = 0.0;
            translateMatrix[2, 2] = 1.0;
            translateMatrix[2, 3] = z;
            translateMatrix[3, 0] = 0.0;
            translateMatrix[3, 1] = 0.0;
            translateMatrix[3, 2] = 0.0;
            translateMatrix[3, 3] = 1.0;
            multiply3(translateMatrix);
        }

        public void rotateX(double a)
        {
            double[,] rotateXMatrix = new double[4, 4];

            a *= Math.PI / 180.0;
            rotateXMatrix[0, 0] = 1.0;
            rotateXMatrix[0, 1] = 0.0;
            rotateXMatrix[0, 2] = 0.0;
            rotateXMatrix[0, 3] = 0.0;
            rotateXMatrix[1, 0] = 0.0;
            rotateXMatrix[1, 1] = Math.Cos(a);
            rotateXMatrix[1, 2] = -Math.Sin(a);
            rotateXMatrix[1, 3] = 0.0;
            rotateXMatrix[2, 0] = 0.0;
            rotateXMatrix[2, 1] = Math.Sin(a);
            rotateXMatrix[2, 2] = Math.Cos(a);
            rotateXMatrix[2, 3] = 0.0;
            rotateXMatrix[3, 0] = 0.0;
            rotateXMatrix[3, 1] = 0.0;
            rotateXMatrix[3, 2] = 0.0;
            rotateXMatrix[3, 3] = 1.0;
            multiply3(rotateXMatrix);
        }

        public void rotateY(double a)
        {
            double[,] rotateYMatrix = new double[4, 4];

            a *= Math.PI / 180.0;
            rotateYMatrix[0, 0] = Math.Cos(a);
            rotateYMatrix[0, 1] = 0.0;
            rotateYMatrix[0, 2] = Math.Sin(a);
            rotateYMatrix[0, 3] = 0.0;
            rotateYMatrix[1, 0] = 0.0;
            rotateYMatrix[1, 1] = 1.0;
            rotateYMatrix[1, 2] = 0.0;
            rotateYMatrix[1, 3] = 0.0;
            rotateYMatrix[2, 0] = -Math.Sin(a);
            rotateYMatrix[2, 1] = 0.0;
            rotateYMatrix[2, 2] = Math.Cos(a);
            rotateYMatrix[2, 3] = 0.0;
            rotateYMatrix[3, 0] = 0.0;
            rotateYMatrix[3, 1] = 0.0;
            rotateYMatrix[3, 2] = 0.0;
            rotateYMatrix[3, 3] = 1.0;
            multiply3(rotateYMatrix);
        }

        public void rotateZ(double a)
        {
            double[,] rotateZMatrix = new double[4, 4];

            a *= Math.PI / 180.0;
            rotateZMatrix[0, 0] = Math.Cos(a);
            rotateZMatrix[0, 1] = -Math.Sin(a);
            rotateZMatrix[0, 2] = 0.0;
            rotateZMatrix[0, 3] = 0.0;
            rotateZMatrix[1, 0] = Math.Sin(a);
            rotateZMatrix[1, 1] = Math.Cos(a);
            rotateZMatrix[1, 2] = 0.0;
            rotateZMatrix[1, 3] = 0.0;
            rotateZMatrix[2, 0] = 0.0;
            rotateZMatrix[2, 1] = 0.0;
            rotateZMatrix[2, 2] = 1.0;
            rotateZMatrix[2, 3] = 0.0;
            rotateZMatrix[3, 0] = 0.0;
            rotateZMatrix[3, 1] = 0.0;
            rotateZMatrix[3, 2] = 0.0;
            rotateZMatrix[3, 3] = 1.0;
            multiply3(rotateZMatrix);
        }

        public void scale(double x, double y, double z)
        {
            double[,] scaleMatrix = new double[4, 4];

            scaleMatrix[0, 0] = x;
            scaleMatrix[0, 1] = 0.0;
            scaleMatrix[0, 2] = 0.0;
            scaleMatrix[0, 3] = 0.0;
            scaleMatrix[1, 0] = 0.0;
            scaleMatrix[1, 1] = y;
            scaleMatrix[1, 2] = 0.0;
            scaleMatrix[1, 3] = 0.0;
            scaleMatrix[2, 0] = 0.0;
            scaleMatrix[2, 1] = 0.0;
            scaleMatrix[2, 2] = z;
            scaleMatrix[2, 3] = 0.0;
            scaleMatrix[3, 0] = 0.0;
            scaleMatrix[3, 1] = 0.0;
            scaleMatrix[3, 2] = 0.0;
            scaleMatrix[3, 3] = 1.0;
            multiply3(scaleMatrix);
        }

    }

}
