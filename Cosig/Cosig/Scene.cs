﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cosig
{
    public class Scene
    {
        //Resolution
        public int[] resolution = new int[2];

        //Background color
        public double[] backgroundColor = new double[3];

        //Constructors
        public Scene(int[] res, double[] color)
        {
            resolution = res;
            backgroundColor = color;
        }
    }
}
