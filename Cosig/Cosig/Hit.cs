﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;

namespace Cosig
{
    public class Hit
    {
        public float t;
        public Vector3D color;

        public Hit(float t, Vector3D color)
        {
            this.t = t;
            this.color = color;
        }
    }
}
