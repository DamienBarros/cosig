﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cosig
{
    public class Material
    {
        //Color
        public double[] color = new double[3];

        //Ambient, diffuse, reflection, refraction (coefficients, refraction)
        public double[] coefficients = new double[5];

        //Constructors
        public Material(double[] col, double[] coef)
        {
            color = col;
            coefficients = coef;
        }

        public double getAmbientCoef()
        {
            return this.coefficients[0];
        }

        public double getDiffuseCoef()
        {
            return this.coefficients[1];
        }

        public double getReflectionCoef()
        {
            return this.coefficients[2];
        }

        public double getRefractionCoef()
        {
            return this.coefficients[3];
        }

        public double getRefraction()
        {
            return this.coefficients[4];
        }
    }
}
