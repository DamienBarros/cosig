﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cosig
{
    class Triangles
    {
        public int transformation;
        public List<Triangle> triangles = new List<Triangle>();

        public Triangles(int T, List<Triangle> triangles)
        {
            transformation = T;
            this.triangles = triangles;
        }
    }
}
