﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;

namespace Cosig
{
    public class Ray
    {
        public Vector3D direction;
        public Vector3D origin;

        public Ray(Vector3D direction, Vector3D origin)
        {
            this.direction = direction;
            this.origin = origin;
        }

        public Vector3D pointAtParameter(double t)
        {
            return origin + direction * t;
        }

    }
}
