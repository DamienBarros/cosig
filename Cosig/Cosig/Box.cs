﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MathNet.Numerics.LinearAlgebra;

namespace Cosig
{
    public class Box : SceneObject
    {
        //Constructor
        public Box(int T, int mat)
        {
            transformation = T;
            material = mat;
        }

        public override void setTMatrix(List<Transformation> transforms, double[,] tcam)
        {
            //Transformations
            Transformatrix matrixGrid = new Transformatrix();
            foreach (Transformation transform in transforms)
            {
                if (transform is Translate)
                {
                    matrixGrid.translate((transform as Translate).getTranslation()[0], (transform as Translate).getTranslation()[1], (transform as Translate).getTranslation()[2]);

                }

                if (transform is Rotate)
                {
                    if (!double.IsNaN((transform as Rotate).getRx()))
                    {
                        matrixGrid.rotateX((transform as Rotate).getRx());
                    }                  
                    if (!double.IsNaN((transform as Rotate).getRy()))
                    {
                        matrixGrid.rotateY((transform as Rotate).getRy());
                    }
                    if (!double.IsNaN((transform as Rotate).getRz()))
                    {
                        matrixGrid.rotateZ((transform as Rotate).getRz());
                    }
                }

                if (transform is Scale)
                {
                    matrixGrid.scale((transform as Scale).getScale()[0], (transform as Scale).getScale()[1], (transform as Scale).getScale()[2]);
                }
            }

            //Obtain final transformation
            Transformatrix auxMatrix = new Transformatrix();
            auxMatrix.setTransformatrix(tcam);
            tmatrix = auxMatrix.multiply3(matrixGrid.getTransformatrix());
            tmatrixInv = Matrix<double>.Build.DenseOfArray(tmatrix).Inverse().ToArray();
            tmatrixInvTrans = Matrix<double>.Build.DenseOfArray(tmatrixInv).Transpose().ToArray();
        }
    }
}
