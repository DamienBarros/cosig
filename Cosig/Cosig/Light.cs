﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Media.Media3D;

namespace Cosig
{
    public class Light
    {
        public double[] intensity = new double[3];
        Point3D origin = new Point3D();
        public int transformation;

        public Light(int T, double[] I)
        {
            transformation = T;
            intensity = I;
        }

        public void setOrigin(List<Transformation> transforms, double[,] tcam)
        {
            //Transformations
            Transformatrix matrixGrid = new Transformatrix();
            foreach (Transformation transform in transforms)
            {
                if (transform is Translate)
                {
                    matrixGrid.translate((transform as Translate).getTranslation()[0], (transform as Translate).getTranslation()[1], (transform as Translate).getTranslation()[2]);

                }

                if (transform is Rotate)
                {
                    if (!double.IsNaN((transform as Rotate).getRx()))
                    {
                        matrixGrid.rotateX((transform as Rotate).getRx());
                    }
                    if (!double.IsNaN((transform as Rotate).getRy()))
                    {
                        matrixGrid.rotateY((transform as Rotate).getRy());
                    }
                    if (!double.IsNaN((transform as Rotate).getRz()))
                    {
                        matrixGrid.rotateZ((transform as Rotate).getRz());
                    }

                }

            }
            Transformatrix auxMatrix = new Transformatrix();
            auxMatrix.setTransformatrix(tcam);
            double[,] TFinal = auxMatrix.multiply3(matrixGrid.getTransformatrix());
            auxMatrix.setTransformatrix(TFinal);

            double[] pLight = auxMatrix.multiply1(new double[] { 0, 0, 0, 1 });
            origin = new Point3D(pLight[0] / pLight[3], pLight[1] / pLight[3], pLight[2] / pLight[3]);
        }

        public Point3D getOrigin()
        {
            return origin;
        }

    }
}
