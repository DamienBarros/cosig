﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cosig
{
    public class Camera
    {
        public int transformation;
        public double distance;
        public double fieldOfView;
        double[,] trmatrix;

        public Camera(int T, double D, double view)
        {
            transformation = T;
            distance = D;
            fieldOfView = view;
        }

        public void setTransformation(List<Transformation> transforms)
        {
            //Transformations
            Transformatrix matrixGrid = new Transformatrix();
            foreach (Transformation transform in transforms)
            {
                if (transform is Translate)
                {
                    matrixGrid.translate((transform as Translate).getTranslation()[0], (transform as Translate).getTranslation()[1], (transform as Translate).getTranslation()[2]);
                }

                if (transform is Rotate)
                {
                    if (!double.IsNaN((transform as Rotate).getRx()))
                    {
                        matrixGrid.rotateX((transform as Rotate).getRx());
                    }
                    if (!double.IsNaN((transform as Rotate).getRy()))
                    {
                        matrixGrid.rotateY((transform as Rotate).getRy());
                    }
                    if (!double.IsNaN((transform as Rotate).getRz()))
                    {
                        matrixGrid.rotateZ((transform as Rotate).getRz());
                    }

                }

            }

            this.trmatrix = matrixGrid.getTransformatrix();
        }
        public double[,] getTMatrix() { return (double[,])this.trmatrix.Clone(); }
    }
}
