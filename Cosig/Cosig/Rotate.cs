﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cosig
{
    class Rotate : Transformation
    {
 
        private double rx;
        private double ry;
        private double rz;

        public Rotate()
        {
            rx = double.NaN;
            ry = double.NaN;
            rz = double.NaN;
        }

        public Rotate(double rx, double ry, double rz)
        {
            this.rx = rx;
            this.ry = ry;
            this.rz = rz;
        }

        public void setRx(double rx)
        {
            this.rx = rx;
        }

        public void setRy(double ry)
        {
            this.ry = ry;
        }

        public void setRz(double rz)
        {
            this.rz = rz;
        }

        public double getRx()
        {
            return rx;
        }

        public double getRy()
        {
            return ry;
        }

        public double getRz()
        {
            return rz;
        }

    }
}
