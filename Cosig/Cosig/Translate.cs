﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cosig
{
    class Translate : Transformation
    {
        private double[] translation = new double[3];

        public Translate(double[] translation)
        {
            this.translation = translation;
        }

        public double[] getTranslation()
        {
            return translation;
        }

        public void setTranslation(double[] translation)
        {
            this.translation = translation;
        }

    }
}
