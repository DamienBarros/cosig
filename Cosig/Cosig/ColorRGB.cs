﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cosig
{
    public class ColorRGB
    {

        private float red;
        private float green;
        private float blue;


        private ColorRGB(float r, float g, float b)
        {
            Red = r;
            Green = g;
            Blue = b;
        }

        public static float Bound(float value)
        {
            return value < 0 ? 0 : (value > 1 ? 1 : value);
        }

        public float Red
        {
            get { return red; }
            private set { red = Bound(value);  }
        }

        public float Green
        {
            get { return green; }
            private set { green = Bound(value); }
        }

        public float Blue
        {
            get { return blue; }
            private set { blue = Bound(value); }
        }

    }
}
